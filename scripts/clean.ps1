$CURRENT = Get-Location

Set-Location $PSScriptRoot
Set-Location "../run"

Remove-Item -Force -Recurse -Path . -Exclude .gitkeep

Set-Location $CURRENT.Path