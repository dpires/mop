$CURRENT = Get-Location

Set-Location $PSScriptRoot
Set-Location ".."

$ROOT_PATH = (Get-Location).Path
$SRC_PATH = Join-Path $ROOT_PATH "diagrams" "src"
$IMG_PATH = Join-Path $ROOT_PATH "docs" "resources"

Get-ChildItem $IMG_PATH |
Foreach-Object {
    Remove-Item $_.FullName
}

Get-ChildItem $SRC_PATH -Filter *.wsd |
Foreach-Object {
    plantuml $_.FullName
    $NAME = $_.BaseName + ".png"
    $DEST = Join-Path $IMG_PATH $NAME
    $SRC = Join-Path $SRC_PATH $NAME
    Move-Item -Path $SRC -Destination $DEST -Force
}

Set-Location $CURRENT.Path