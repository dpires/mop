@startuml architecture
package "Library" {
    [Media Importer]
    [Media Files]
    [Metadata Database]
    [Library Settings]
    [Library Query]
}

package "Metadata Crawler" {
    [Media Metadata]
    [Metadata Info]
    [Lyrics]
    [Album Covers]
    [Crawler Settings]
    [Extra Info]
}

package "Player" {
    [Media Player] ..> [Player Settings]
}

package "Core" {
    [Settings]
    [Importer]
    [Query]
}

[Media Importer] --> [Media Files]
[Media Importer] --> [Metadata Database]
[Library Query] --> [Media Files]
[Library Query] --> [Metadata Database]
[Media Importer] ..> [Library Settings]
[Media Files] ..> [Library Settings]

[Media Metadata] --> [Metadata Info]
[Media Metadata] --> [Lyrics]
[Media Metadata] --> [Album Covers]
[Media Metadata] --> [Extra Info]
[Media Metadata] ..> [Crawler Settings]

[Media Player] ..> [Query]

[Importer] --> [Media Importer]
[Importer] ...> [Media Metadata]
[Query] --> [Library Query]

[Interface] -> [Settings]
[Interface] -> [Importer]
[Interface] -> [Query]
[Interface] -> [Media Player]
@enduml