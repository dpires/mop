# Metadata Storage

Module responsible to insert and browse metadata

## Architecture overview

![Image](resources/metadata-storage.png)

## Description

**MetadataStorageHandler** :

    Is the entrypoint for external actors to write/read metadata.
    It will generate events from messages, and create projections of proccessed data.
    When a read is required, values are returned from ProjectionsDatabase

**MetadataEvents** :

    Process all events related to metadata

**EventsDatabase** :

    Store all events proccessed by [MetadataEvents]

**MetadataProjection** :

    Generate projections from EventsDatabase

**MetadataProjectionDatabase** :

    Store generated projections

**MetadataBrowser** :

    Return results for queries to metadata