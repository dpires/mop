using Mop.Modules;
using Xunit;

namespace Mop.Test.Modules
{
    public class MopActorSystemTests
    {
        [Fact]
        public void TestSystemInitialization()
        {
            var actorSystem = MopActorSystem.Initialize();

            Assert.NotNull(actorSystem);
        }
    }
}