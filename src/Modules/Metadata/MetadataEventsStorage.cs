using System.Collections.Generic;
using Akka.Actor;
using Akka.Event;
using LiteDB;
using Mop.Core.Modules.Metadata;
using Mop.Core.Modules.Metadata.Storage;

namespace Mop.Modules.Metadata
{
    /// <summary>
    /// Metadata Events database handler
    /// </summary>
    public class MetadataEventsStorage : ReceiveActor
    {
        public string DatabasePath { get; private set; }

        private readonly ILoggingAdapter _log = Context.GetLogger();
        private LiteDatabase? _db;
        private ILiteCollection<MetadataEvent>? _collection;

        public MetadataEventsStorage(string databasePath)
        {
            DatabasePath = databasePath;
            RegisterReceivers();
        }

        protected override void PreStart()
        {
            var connection = $"Filename={DatabasePath};Connection=direct";
            _db = new LiteDatabase(DatabasePath);
            _collection = _db.GetCollection<MetadataEvent>();
            _collection.EnsureIndex(e => e.Id);
        }

        private void RegisterReceivers() {
            Receive<MetadataWriteEvent>(OnWriteRequest);
            Receive<MetadataReadEvent>(OnReadRequest);
            ReceiveAny(OnUnkownMessage);
        }

        private void OnWriteRequest(MetadataWriteEvent writeEvent) {
            var id = _collection?.Insert(writeEvent.Event);
            switch (id?.AsGuid == writeEvent.Event._id)
            {
                case true: 
                    _log.Info("Inserted new event: {0}", writeEvent.Event._id); 
                    break;
                case false:
                    _log.Error("Error inserting value");
                    break;
            }
        }

        private void OnReadRequest(MetadataReadEvent readEvent) {
            IEnumerable<MetadataEvent> query = _collection?
                .Find(e => e.Created > readEvent.FromDate)
                ?? new List<MetadataEvent>();

            Sender.Tell(query);
        }

        private void OnUnkownMessage(object message) {
            var errorMessage = $"MetadataEventStorage: Received unknown message type: {message.GetType()}";
            _log.Error(errorMessage);
        }

        public static Props Props(string databasePath) {
            return Akka.Actor.Props.Create<MetadataEventsStorage>(databasePath);
        }
    }
}