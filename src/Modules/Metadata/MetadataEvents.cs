#nullable enable
using Akka.Actor;
using Akka.Event;
using Mop.Core.Modules.Metadata;
using Mop.Core.Modules.Metadata.Storage;

using static Mop.Core.MopActorNames;

namespace Mop.Modules.Metadata
{
    public class MetadataEvents : ReceiveActor
    {
        public string DatabasePath { get; private set; }

        private readonly ILoggingAdapter _log = Context.GetLogger();
        private IActorRef? _storage;

        public MetadataEvents(string databasePath)
        {
            DatabasePath = databasePath;
        }

        protected override void PreStart() {
            _storage = Context.ActorOf(MetadataEventsStorage.Props(DatabasePath), METADATA_EVENTS_STORAGE);
        }

        private void OnMetadataEvent(IMetadataEvent eventMessage) {
            var eventValue = MetadataEvent.FromInterface(eventMessage, true);
            _storage?.Tell(new MetadataWriteEvent(eventValue));
        }

        public static Props Props(string databasePath) {
            return Akka.Actor.Props.Create<MetadataEvents>(databasePath);
        }
    }
}