#nullable enable
using Akka.Actor;

using static Mop.Core.MopActorNames;

namespace Mop.Modules.Metadata
{
    /// <summary>
    /// Main metadata actor
    /// </summary>
    public class MetadaMain : UntypedActor
    {
        private IActorRef? _events;

        protected override void PreStart()
        {
            _events = Context.ActorOf(MetadataEvents.Props("events.db"), METADATA_EVENTS);
        }

        protected override void OnReceive(object message)
        {
            _events?.Tell(message);
        }

        public static Props Props() {
            return Akka.Actor.Props.Create<MetadaMain>();
        }
    }
}