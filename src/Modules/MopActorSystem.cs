using Akka.Actor;
using Mop.Core.Modules.Metadata;
using Mop.Modules.Metadata;

using static Mop.Core.MopActorNames;

namespace Mop.Modules
{
    public class MopActorSystem
    {
        public static ActorSystem Initialize()
        {
            var system = ActorSystem.Create(MAIN_SYSTEM);

            var ActorRef = system.ActorOf(MetadaMain.Props(), METADATA_MAIN);
            ActorRef.Tell(new MetadataEventMessage("target", MetadataEventActionName.Delete));

            return system;
        }
    }
}