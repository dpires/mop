﻿namespace Mop.Core
{
    /// <summary>
    /// Main Actor names
    /// </summary>
    public static class MopActorNames
    {
        /// <summary>
        /// Main actor
        /// </summary>
        public const string MAIN_SYSTEM = "MopActorSystem";

        /// <summary>
        /// Entry point for the metadata actor
        /// </summary>
        public const string METADATA_MAIN = "MopActorMetadata";
        /// <summary>
        /// Actor handle events
        /// </summary>
        public const string METADATA_EVENTS = "MopActorMetadataEvents";
        /// <summary>
        /// Actor to store events
        /// </summary>
        public const string METADATA_EVENTS_STORAGE = "MopActorMetadataEventsStorage";
        /// <summary>
        /// Process events and project result to a database
        /// </summary>
        public const string METADATA_EVENTS_PROJECTION = "MopActorMetadataEventsProjection";
    }
}