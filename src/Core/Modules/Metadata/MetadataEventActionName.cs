namespace Mop.Core.Modules.Metadata
{
    public class MetadataEventActionName
    {
        /// <summary>Set current value, old values are replaced</summary>
        public const string Set = "SET";
        /// <summary>add value, old values are kept</summary>
        public const string Add = "ADD";
        /// <summary>remove value, other values are kept</summary>
        public const string Delete = "DELETE";
        /// <summary>remove all values</summary>
        public const string Empty = "DELETE_ALL";
    }
}