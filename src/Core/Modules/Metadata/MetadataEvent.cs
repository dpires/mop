#nullable enable
using System;
using System.Collections.Generic;
using LiteDB;

namespace Mop.Core.Modules.Metadata
{
    /// <summary>
    /// Message to be stored in event db
    /// </summary>
    public class MetadataEvent : IMetadataEvent
    {
        public Guid _id = Guid.NewGuid();
        public Guid Id
        {
            get => _id;
            set => _id = value;
        }

        public string Target { get; set; } = "UNKOWN";

        public List<(string Key, string Value)> Values { get; set; } =
            new List<(string Key, string Value)>();

        public string Action { get; set; } = "UNKOWN";

        public DateTime Created { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// Create instance from interface
        /// </summary>
        /// <param name="meta">interface to create from</param>
        /// <param name="updateTime">If true, [Created] value is set to current time</param>
        /// <returns></returns>
        public static MetadataEvent FromInterface(IMetadataEvent meta, bool updateTime = false)
        {
            return new MetadataEvent
            {
                Id = meta.Id,
                Target = meta.Target,
                Action = meta.Action,
                Values = meta.Values,
                Created = updateTime ? DateTime.UtcNow : meta.Created
            };
        }
    }
}