namespace Mop.Core.Modules.Metadata.Storage
{
    public class MetadataWriteEvent
    {
        public MetadataEvent Event { get; set; }

        public MetadataWriteEvent(MetadataEvent metadataEvent) {
            Event = metadataEvent;
        }
    }
}