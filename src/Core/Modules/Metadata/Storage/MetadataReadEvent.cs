using System;

namespace Mop.Core.Modules.Metadata.Storage
{
    /// <summary>
    /// Event to request events
    /// </summary>
    public class MetadataReadEvent
    {
        public DateTime FromDate { get; set; }
    }
}