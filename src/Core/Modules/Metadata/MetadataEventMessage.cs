#nullable enable
using System;
using System.Collections.Generic;

namespace Mop.Core.Modules.Metadata
{
    /// <summary> Message to change metadata values for a file </summary>
    public class MetadataEventMessage : IMetadataEvent
    {
        /// <summary> Event unique id </summary>
        public Guid Id { get; } = Guid.NewGuid();

        /// <summary> File target </summary>
        public string Target { get; private set; }

        /// <summary> List of values to update </summary>
        public List<(string Key, string Value)> Values { get; }

        /// <summary> Action of event </summary>
        public string Action { get; private set; }
        
        public DateTime Created => DateTime.UtcNow;

        public MetadataEventMessage(string target, string action, List<(string Key, string Value)>? values = null)
        {
            Target = target;
            Action = action;
            Values = values ?? new List<(string Key, string Value)>();
        }

    }
}