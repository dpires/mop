#nullable enable
using System;
using System.Collections.Generic;

namespace Mop.Core.Modules.Metadata
{
    public interface IMetadataEvent
    {
        /// <summary> Event unique id </summary>
        Guid Id { get; }
        /// <summary> File target </summary>
        string Target { get; }
        /// <summary> List of values to update </summary>
        List<(string Key, string Value)> Values { get; }
        /// <summary> Action of event </summary>
        string Action { get; }
        /// <summary> Creation date </summary>
        DateTime Created { get; }
    }
}