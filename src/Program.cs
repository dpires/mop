﻿#nullable enable
using Akka.Actor;
using static Mop.Modules.MopActorSystem;

namespace Mop
{
    class Program
    {
        public static ActorSystem? MopActorSystem;

        static void Main(string[] args)
        {
            MopActorSystem = Initialize();
            MopActorSystem.WhenTerminated.Wait();
        }
    }
}
